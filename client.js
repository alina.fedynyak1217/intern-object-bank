import { clientsArr } from "./clientArr.js";
import { Form } from "./form.js";

class Client {
    constructor() {
        let clientItems = JSON.parse(localStorage.getItem('client'));
        this.client = clientItems !== null ? clientItems : clientsArr;
        this.init(this.client);
        this.formObj = new Form();
        this.formObj.createForm();
        this.bindSaveClient = this.saveClient.bind(this);
        this.bindSaveChangedClient = this.saveChangedClient.bind(this)
        let saveNewClientBtn = document.getElementById('btn-save');
        saveNewClientBtn.addEventListener('click', this.bindSaveClient);
    }

    init() {
        this.createCartHtml(this.client);
        localStorage.setItem('client', JSON.stringify(this.client));
    }

    createCartHtml() {
        let cartContainer = document.getElementById('clients');
        cartContainer.className = 'cart';

        this.client.forEach((itemClient) => {
            let cartClient = document.createElement('div');
            cartClient.className = 'cart__clients';
            let listClient = document.createElement('ul');
            listClient.className = 'list__clients';

            for (let item in itemClient) {
                if (item !== 'id' && item !== 'cart') {
                    let fieldClient = document.createElement('li');
                    fieldClient.innerText = item + ': ' + itemClient[item];
                    listClient.appendChild(fieldClient);
                }
            }

            let blockBankCarts = document.createElement('div');
            blockBankCarts.className = 'bank-carts';
            itemClient.cart.forEach(cart => {

                let blockCart = document.createElement('div');
                blockCart.className = 'carts';

                let titleCart = document.createElement('h2');
                titleCart.innerText = cart.typeCart;

                let countMoneyInfo = document.createElement('div');
                countMoneyInfo.innerText = 'countMoney: ' + cart.countMoney;
                let expairetAtInfo = document.createElement('div');
                expairetAtInfo.innerText = 'expairetAt: ' + cart.expairetAt;
                let lastOperationInfo = document.createElement('div');
                lastOperationInfo.innerText = 'lastOperation: ' + cart.lastOperation;
                let currencyInfo = document.createElement('div');
                currencyInfo.innerText = 'currency: ' + cart.currency;
                blockCart.appendChild(titleCart);
                blockCart.appendChild(countMoneyInfo);
                blockCart.appendChild(expairetAtInfo);
                blockCart.appendChild(lastOperationInfo);
                blockCart.appendChild(currencyInfo);

                if (cart.typeCart === 'credit') {
                    let creditLimitInfo = document.createElement('div');
                    creditLimitInfo.innerText = 'creditLimit: ' + cart.creditLimit;
                    let creditCountMoney = document.createElement('div');
                    creditCountMoney.innerText = 'creditCountMoney: ' + cart.creditCountMoney;

                    blockCart.appendChild(creditLimitInfo);
                    blockCart.appendChild(creditCountMoney);
                }

                listClient.appendChild(blockBankCarts);
                blockBankCarts.appendChild(blockCart);

            });

            let cartEvent = document.createElement('div');
            cartEvent.className = 'cart__event';

            let btnEdit = document.createElement('button');
            btnEdit.id = 'btn-edit';
            btnEdit.className = 'btn-edit';
            btnEdit.innerText = 'Edit';
            btnEdit.setAttribute('data-client-id', itemClient.id);
            btnEdit.addEventListener('click', this.editCart.bind(this));

            let btnDelete = document.createElement('button');
            btnDelete.id = 'btn-delete';
            btnDelete.className = 'btn-delete';
            btnDelete.innerText = 'Delete';
            btnDelete.setAttribute('data-client-id', itemClient.id);
            btnDelete.addEventListener('click', this.deleteCart.bind(this));

            cartEvent.appendChild(btnEdit);
            cartEvent.appendChild(btnDelete);

            cartClient.appendChild(listClient);
            cartClient.appendChild(blockBankCarts);
            cartClient.appendChild(cartEvent);
            cartContainer.appendChild(cartClient);
        })
    }

    clearCartsBlock() {
        let cartsBlock = document.getElementById('clients');
        cartsBlock.innerText = '';
    }

    editCart(event) {
        let clientId = event.currentTarget.getAttribute('data-client-id');
        let clients = JSON.parse(localStorage.getItem('client'));

        this.client = clients !== null ? clients : [];
        let editClient = this.client.filter((person) => {
            if (person.id === clientId) {
                return true;
            }
        }).shift();

        let inputName = document.getElementById('name');
        inputName.value = editClient.name;
        let inputSurname = document.getElementById('surname');
        inputSurname.value = editClient.surname;
        let checkboxIsActive = document.getElementById('isActive');
        checkboxIsActive.checked = editClient.isActive;
        let inputRegistration = document.getElementById('registration');
        inputRegistration.value = editClient.registration;

        editClient.cart.forEach((cart) => {
            if (cart.typeCart === 'debit'){
                let debitCheckbox = document.getElementById('debit');
                debitCheckbox.checked = cart.typeCart;
                let debitCountMoneyInput = document.getElementById('debitCountMoney');
                debitCountMoneyInput.value = cart.countMoney;
                let debitExpairetAtInput = document.getElementById('debitExpairetAt');
                debitExpairetAtInput.value = cart.expairetAt;
                let debitLastOperationLabel = document.getElementById('debitLastOperation');
                debitLastOperationLabel.value = cart.lastOperation;
                let debitCurrencySelect = document.getElementById('debitCurrency');
                debitCurrencySelect.value = cart.currency;
            }
            if (cart.typeCart === 'credit') {
                let creditCheckbox = document.getElementById('credit');
                creditCheckbox.checked = cart.typeCart;
                let creditCountMoneyInput = document.getElementById('countMoney');
                creditCountMoneyInput.value = cart.countMoney;
                let creditExpairetAtInput = document.getElementById('creditExpairetAt');
                creditExpairetAtInput.value = cart.expairetAt;
                let creditLastOperationLabel = document.getElementById('creditLastOperation');
                creditLastOperationLabel.value = cart.lastOperation;
                let creditLimit = document.getElementById('creditLimit');
                creditLimit.value = cart.creditLimit;
                let creditCountMoney = document.getElementById('creditCountMoney');
                creditCountMoney.value = cart.creditCountMoney;
                let creditCurrencySelect = document.getElementById('creditCurrency');
                creditCurrencySelect.value = cart.currency;
            }
        });

        let saveBtn = document.getElementById('btn-save');
        saveBtn.setAttribute('data-client-id', editClient.id);
        saveBtn.addEventListener('click', this.bindSaveChangedClient);
        saveBtn.removeEventListener('click', this.bindSaveClient);
    }

    deleteCart(event) {
        let clientId = event.currentTarget.getAttribute('data-client-id');
        let clients = JSON.parse(localStorage.getItem('client'));

        this.client = clients.filter((person) => {
            if (person.id !== clientId) {
                return true;
            }
        });

        localStorage.setItem('client', JSON.stringify(this.client));
        this.clearCartsBlock();
        this.init();
    }

    //сохраняет изменного сотрудника в карточку
    saveChangedClient(event) {
        event.preventDefault();
        let clientId = event.currentTarget.getAttribute('data-client-id');
        let clients = JSON.parse(localStorage.getItem('client'));

        let form = document.querySelector('form');
        let formData = new FormData(form);

        this.client = clients.map((person) => {
            if (person.id === clientId) {
                person.name = formData.get('name');
                person.surname = formData.get('surname');
                person.isActive = formData.get('isActive');
                person.registration = formData.get('registration');

                person.cart.forEach((cart) => {
                        if (cart.typeCart === 'credit') {
                            cart.countMoney = formData.get('debitCountMoney');
                            cart.expairetAt = formData.get('debitExpairetAt');
                            cart.lastOperation = formData.get('debitLastOperation');
                            cart.currency = formData.get('debitCurrency');
                        }
                        if (cart.typeCart === 'credit') {
                            cart.countMoney = formData.get('creditCountMoney');
                            cart.expairetAt = formData.get('creditExpairetAt');
                            cart.lastOperation = formData.get('creditLastOperation');
                            cart.creditLimit = formData.get('creditLimit');
                            cart.creditCountMoney = formData.get('creditCountMoney');
                            cart.currency = formData.get('creditCurrency');
                        }
                    }
                )
            }
            return person;
        });

        form.reset();
        let saveBtn = document.getElementById('btn-save');
        saveBtn.removeAttribute('data-client-id');
        saveBtn.addEventListener('click', this.bindSaveClient);
        saveBtn.removeEventListener('click', this.bindSaveChangedClient);

        localStorage.setItem('client', JSON.stringify(this.client));
        this.clearCartsBlock();
        this.init();
    }

    //сохраняет нового сотрудника в карточку
    saveClient(event) {
        event.preventDefault();
        let form = document.querySelector('form');
        let formData = new FormData(form);

        this.client = JSON.parse(localStorage.getItem('client'));

        let newClient = {
            id: crypto.randomUUID(),
            name: formData.get('name'),
            surname: formData.get('surname'),
            isActive: formData.get('isActive'),
            registration: formData.get('registration'),
            cart: []
        };

        if (formData.get('debit') === 'debit') {
            let debitCart = {
                    typeCart: formData.get('debit'),
                    countMoney: formData.get('debitCountMoney'),
                    expairetAt: formData.get('debitExpairetAt'),
                    lastOperation: formData.get('debitLastOperation'),
                    currency: formData.get('debitCurrency'),
                };
            newClient.cart.push(debitCart)
        }

        if (formData.get('credit') === 'credit') {
            let creditCart = {
                typeCart: formData.get('credit'),
                countMoney: formData.get('creditCountMoney'),
                expairetAt: formData.get('creditExpairetAt'),
                lastOperation: formData.get('creditLastOperation'),
                creditLimit: formData.get('creditLimit'),
                creditCountMoney: formData.get('creditCountMoney'),
                currency: formData.get('creditCurrency'),
            };
            newClient.cart.push(creditCart);
        }

        form.reset();

        this.client.push(newClient);
        this.clearCartsBlock();
        this.init();
    }

}
let client = new Client(clientsArr);

document.addEventListener("DOMContentLoaded", function(event) {
});
