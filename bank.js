import { clientsArr } from "./clientArr.js";

class Bank {
    constructor(client) {
        this.name = client.name;
        this.surname = client.surname;
        this.active = client.active;
        this.registration = client.registration;
        this.cart = client.cart;
        this.client = clientsArr;
    }

    sumAllMoney() {
        let sum = 0;
        for (let i = 0; i < this.client.length; i++) {
            for (let j = 0; j < this.client[i].cart.length; j++) {
                sum += this.client[i].cart[j].countMoney;
                this.client[i].cart[j].history.forEach((item) => {
                    sum += item.money;
                });

                if (this.client[i].cart[j].typeCart === 'credit') {
                    sum += this.client[i].cart[j].creditLimit;
                }
            }
        }

        return sum;
    }

    sumDebt() {
        let sum = 0;

        this.client.forEach(person => {
            person.cart.forEach(cart => {
                if (cart.typeCart === 'credit') {
                    sum += cart.creditCountMoney;
                }
            })
        });

        return sum;
    }

    getCountDebt(callback) {
        let sum = 0;

        let clientDebt = this.client.filter(person => {
            return callback(person.isActive);
        });
        clientDebt.forEach(person => {
            person.cart.forEach(cart => {
                if (cart.typeCart === 'credit') {
                    sum += cart.creditCountMoney;
                }
            })
        });

        return sum;
    }

    getCurrency(currency) {
        return fetch("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5")
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                    let currencyData = {};
                    data.forEach((item, index) => {
                        currencyData[data[index].ccy] = data[index].buy;
                    });
                    return currencyData[currency];
                }
            )
            .catch(error => ('error', error));
    }

     getConvert(currency) {
        let value = this.getCurrency(currency);

        let money = 0;
        return value.then(data => {
            money = this.sumAllMoney() / Number(data);
            return parseFloat(money.toFixed(2));
        });
    }
}
const bank = new Bank(clientsArr);

bank.sumAllMoney();
bank.sumDebt();
bank.getCountDebt((value) => value === false);
bank.getConvert('USD');
