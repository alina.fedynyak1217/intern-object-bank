export class Form {
    createForm() {
        let form = document.getElementById('form-bank');
        if (form.length !== undefined) {
            return;
        }

        let formCart = document.createElement('form');
        formCart.className = 'form';
        formCart.id = 'form';
        let formTitle = document.createElement('h1');
        formTitle.innerText = 'Form Add';
        let blockName = document.createElement('div');
        let nameLabel = document.createElement('label');
        nameLabel.for = 'name';
        nameLabel.innerText = 'Name:';
        let nameInput = document.createElement('input');
        nameInput.type = 'text';
        nameInput.name = 'name';
        nameInput.id = 'name';
        nameInput.value = '';

        blockName.appendChild(nameLabel);
        blockName.appendChild(nameInput);

        formCart.appendChild(formTitle);
        formCart.appendChild(blockName);

        let blockSurname = document.createElement('div');
        let surnameLabel = document.createElement('label');
        surnameLabel.for = 'surname';
        surnameLabel.innerText = 'Surname:';
        let surnameInput = document.createElement('input');
        surnameInput.type = 'text';
        surnameInput.name = 'surname';
        surnameInput.id = 'surname';
        surnameInput.value = '';

        blockSurname.appendChild(surnameLabel);
        blockSurname.appendChild(surnameInput);
        formCart.appendChild(blockSurname);

        let blockIsActive = document.createElement('div');
        blockIsActive.className = 'checkbox';
        let blockIsActiveInput = document.createElement('input');
        blockIsActiveInput.type = 'checkbox';
        blockIsActiveInput.name = 'isActive';
        blockIsActiveInput.id = 'isActive';
        let blockIsActiveLabel = document.createElement('label');
        blockIsActiveLabel.for = 'isActive';
        blockIsActiveLabel.innerText = 'isActive';

        blockIsActive.appendChild(blockIsActiveInput);
        blockIsActive.appendChild(blockIsActiveLabel);
        formCart.appendChild(blockIsActive);

        let blockRegistration = document.createElement('div');
        let registrationLabel = document.createElement('label');
        registrationLabel.for = 'registration';
        registrationLabel.innerText = 'Registration:';
        let registrationInput = document.createElement('input');
        registrationInput.type = 'date';
        registrationInput.name = 'registration';
        registrationInput.id = 'registration';
        registrationInput.value = '';

        blockRegistration.appendChild(registrationLabel);
        blockRegistration.appendChild(registrationInput);
        formCart.appendChild(blockRegistration);

        let blockCarts = document.createElement('div');
        let blockCartsTitle = document.createElement('h2');
        blockCartsTitle.innerText = 'Carts:';
        blockCartsTitle.id = 'title-cart';

        blockCarts.appendChild(blockCartsTitle);
        formCart.appendChild(blockCarts);

        let blockDebit = document.createElement('div');
        blockDebit.className = 'debit-block';
        let debitCheckbox = document.createElement('div');
        debitCheckbox.className = 'checkbox';
        let debitInput = document.createElement('input');
        debitInput.type = 'checkbox';
        debitInput.name = 'debit';
        debitInput.id = 'debit';
        debitInput.value = 'debit';
        let debitLabel = document.createElement('label');
        debitLabel.for = 'debit';
        debitLabel.innerText = 'Debit:';

        debitCheckbox.appendChild(debitInput);
        debitCheckbox.appendChild(debitLabel);
        blockDebit.appendChild(debitCheckbox);

        let blockDebitCountMoney = document.createElement('div');
        let debitCountMoneyInput = document.createElement('input');
        debitCountMoneyInput.type = 'text';
        debitCountMoneyInput.name = 'debitCountMoney';
        debitCountMoneyInput.id = 'debitCountMoney';
        debitCountMoneyInput.value = '';
        let debitCountMoneyLabel = document.createElement('label');
        debitCountMoneyLabel.for = 'debitCountMoney';
        debitCountMoneyLabel.innerText = 'Count Money:';

        blockDebitCountMoney.appendChild(debitCountMoneyLabel);
        blockDebitCountMoney.appendChild(debitCountMoneyInput);
        blockDebit.appendChild(blockDebitCountMoney);

        let blockDebitExpairetAt = document.createElement('div');
        let debitExpairetAtInput = document.createElement('input');
        debitExpairetAtInput.type = 'date';
        debitExpairetAtInput.name = 'debitExpairetAt';
        debitExpairetAtInput.id = 'debitExpairetAt';
        debitExpairetAtInput.value = '';
        let debitExpairetAtLabel = document.createElement('label');
        debitExpairetAtLabel.for = 'debitExpairetAt';
        debitExpairetAtLabel.innerText = 'Expairet At:';

        blockDebitExpairetAt.appendChild(debitExpairetAtLabel);
        blockDebitExpairetAt.appendChild(debitExpairetAtInput);
        blockDebit.appendChild(blockDebitExpairetAt);

        let blockDebitLastOperation = document.createElement('div');
        let debitLastOperationInput = document.createElement('input');
        debitLastOperationInput.type = 'date';
        debitLastOperationInput.name = 'debitLastOperation';
        debitLastOperationInput.id = 'debitLastOperation';
        debitLastOperationInput.value = '';
        let debitLastOperationLabel = document.createElement('label');
        debitLastOperationLabel.for = 'debitLastOperation';
        debitLastOperationLabel.innerText = 'Last Operation:';

        blockDebitLastOperation.appendChild(debitLastOperationLabel);
        blockDebitLastOperation.appendChild(debitLastOperationInput);
        blockDebit.appendChild(blockDebitLastOperation);

        let blockDebitCurrency = document.createElement('div');
        let debitCurrencyLabel = document.createElement('label');
        debitCurrencyLabel.for = 'debitCurrency';
        debitCurrencyLabel.innerText = 'currency:';
        let debitCurrencySelect = document.createElement('select');
        debitCurrencySelect.id = 'debitCurrency';
        debitCurrencySelect.name = 'debitCurrency';
        let debitOptionUSD = document.createElement('option');
        debitOptionUSD.value = 'USD';
        debitOptionUSD.innerText = 'USD';
        let debitOptionRUB = document.createElement('option');
        debitOptionRUB.value = 'RUB';
        debitOptionRUB.innerText = 'RUB';
        let debitOptionUAH = document.createElement('option');
        debitOptionUAH.value = 'UAH';
        debitOptionUAH.innerText = 'UAH';
        let debitOptionEUR = document.createElement('option');
        debitOptionEUR.value = 'EUR';
        debitOptionEUR.innerText = 'EUR';

        blockDebitCurrency.appendChild(debitCurrencyLabel);
        debitCurrencySelect.appendChild(debitOptionUSD);
        debitCurrencySelect.appendChild(debitOptionRUB);
        debitCurrencySelect.appendChild(debitOptionUAH);
        debitCurrencySelect.appendChild(debitOptionEUR);
        blockDebitCurrency.appendChild(debitCurrencySelect);
        blockDebit.appendChild(blockDebitCurrency);
        formCart.appendChild(blockDebit);

        let blockCredit = document.createElement('div');
        blockCredit.className = 'credit-block';
        let creditCheckbox = document.createElement('div');
        creditCheckbox.className = 'checkbox';
        let creditInput = document.createElement('input');
        creditInput.type = 'checkbox';
        creditInput.name = 'credit';
        creditInput.id = 'credit';
        creditInput.value = 'credit';
        let creditLabel = document.createElement('label');
        creditLabel.for = 'credit';
        creditLabel.innerText = 'Credit:';

        creditCheckbox.appendChild(creditInput);
        creditCheckbox.appendChild(creditLabel);
        blockCredit.appendChild(creditCheckbox);

        let blockCountMoney = document.createElement('div');
        let countMoneyInput = document.createElement('input');
        countMoneyInput.type = 'text';
        countMoneyInput.name = 'countMoney';
        countMoneyInput.id = 'countMoney';
        countMoneyInput.value = '';
        let countMoneyLabel = document.createElement('label');
        countMoneyLabel.for = 'countMoney';
        countMoneyLabel.innerText = 'Count Money:';

        blockCountMoney.appendChild(countMoneyLabel);
        blockCountMoney.appendChild(countMoneyInput);
        blockCredit.appendChild(blockCountMoney);

        let blockCreditExpairetAt = document.createElement('div');
        let creditExpairetAtInput = document.createElement('input');
        creditExpairetAtInput.type = 'date';
        creditExpairetAtInput.name = 'creditExpairetAt';
        creditExpairetAtInput.id = 'creditExpairetAt';
        creditExpairetAtInput.value = '';
        let creditExpairetAtLabel = document.createElement('label');
        creditExpairetAtLabel.for = 'creditExpairetAt';
        creditExpairetAtLabel.innerText = 'Expairet At:';

        blockCreditExpairetAt.appendChild(creditExpairetAtLabel);
        blockCreditExpairetAt.appendChild(creditExpairetAtInput);
        blockCredit.appendChild(blockCreditExpairetAt);

        let blockCreditLastOperation = document.createElement('div');
        let creditLastOperationInput = document.createElement('input');
        creditLastOperationInput.type = 'date';
        creditLastOperationInput.name = 'creditLastOperation';
        creditLastOperationInput.id = 'creditLastOperation';
        creditLastOperationInput.value = '';
        let creditLastOperationLabel = document.createElement('label');
        creditLastOperationLabel.for = 'creditLastOperation';
        creditLastOperationLabel.innerText = 'Last Operation:';

        blockCreditLastOperation.appendChild(creditLastOperationLabel);
        blockCreditLastOperation.appendChild(creditLastOperationInput);
        blockDebit.appendChild(blockCreditLastOperation);

        let blockCreditLimit = document.createElement('div');
        let creditLimitInput = document.createElement('input');
        creditLimitInput.type = 'text';
        creditLimitInput.name = 'creditLimit';
        creditLimitInput.id = 'creditLimit';
        creditLimitInput.value = '';
        let creditLimitLabel = document.createElement('label');
        creditLimitLabel.for = 'creditLimit';
        creditLimitLabel.innerText = 'Сredit Limit:';

        blockCreditLimit.appendChild(creditLimitLabel);
        blockCreditLimit.appendChild(creditLimitInput);
        blockCredit.appendChild(blockCreditLimit);

        let blockCreditCountMoney = document.createElement('div');
        let creditCountMoneyInput = document.createElement('input');
        creditCountMoneyInput.type = 'text';
        creditCountMoneyInput.name = 'creditCountMoney';
        creditCountMoneyInput.id = 'creditCountMoney';
        creditCountMoneyInput.value = '';
        let creditCountMoneyLabel = document.createElement('label');
        creditCountMoneyLabel.for = 'creditCountMoney';
        creditCountMoneyLabel.innerText = 'Сredit Count Money:';

        blockCreditCountMoney.appendChild(creditCountMoneyLabel);
        blockCreditCountMoney.appendChild(creditCountMoneyInput);
        blockCredit.appendChild(blockCreditCountMoney);

        let blockCreditCurrency = document.createElement('div');
        let creditCurrencyLabel = document.createElement('label');
        creditCurrencyLabel.for = 'creditCurrency';
        creditCurrencyLabel.innerText = 'currency:';
        let creditCurrencySelect = document.createElement('select');
        creditCurrencySelect.id = 'creditCurrency';
        creditCurrencySelect.name = 'creditCurrency';
        let creditOptionUSD = document.createElement('option');
        creditOptionUSD.value = 'USD';
        creditOptionUSD.innerText = 'USD';
        let creditOptionRUB = document.createElement('option');
        creditOptionRUB.value = 'RUB';
        creditOptionRUB.innerText = 'RUB';
        let creditOptionUAH = document.createElement('option');
        creditOptionUAH.value = 'UAH';
        creditOptionUAH.innerText = 'UAH';
        let creditOptionEUR = document.createElement('option');
        creditOptionEUR.value = 'EUR';
        creditOptionEUR.innerText = 'EUR';

        blockCreditCurrency.appendChild(creditCurrencyLabel);
        creditCurrencySelect.appendChild(creditOptionUSD);
        creditCurrencySelect.appendChild(creditOptionRUB);
        creditCurrencySelect.appendChild(creditOptionUAH);
        creditCurrencySelect.appendChild(creditOptionEUR);
        blockCreditCurrency.appendChild(creditCurrencySelect);
        blockCredit.appendChild(blockCreditCurrency);
        blockDebit.appendChild(blockCredit);
        formCart.appendChild(blockCredit);


        let blockBtn = document.createElement('div');
        blockBtn.className = 'btn-add';
        blockBtn.id = 'btn-save';
        let buttonAdd = document.createElement('button');
        buttonAdd.className = 'add';
        buttonAdd.innerText = 'Add';

        blockBtn.appendChild(buttonAdd);
        formCart.appendChild(blockBtn);
        form.appendChild(formCart);
    }
}