export const clientsArr = [
    {
        id: 'bda0d2ef-564c-46c8-b7c5-317eb494b908',
        name: 'Федя',
        surname: 'Хрущ',
        isActive: true,
        registration: '2021-10-11',
        cart: [
            {
                typeCart: 'debit',
                countMoney: 3564,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 250,
                        historyСountMoney: 3314
                    },
                    {
                        time: '2021-25-14',
                        money: 500,
                        historyСountMoney: 2814
                    }
                ]
            },
            {
                typeCart: 'credit',
                countMoney: 2500,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                creditLimit: 50000,
                creditCountMoney: 5,
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 256,
                        historyСountMoney: 2244
                    },
                    {
                        time: '2021-25-14',
                        money: 500,
                        historyСountMoney: 1744
                    }
                ]
            }
        ]
    },
    {
        id: 'c78da06b-7fd0-4807-a378-493e3b45e0fb',
        name: 'Кеша',
        surname: 'Хрущ',
        isActive: true,
        registration: '2021-10-11',
        cart: [
            {
                typeCart: 'debit',
                countMoney: 3564,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 250,
                        historyСountMoney: 3314
                    },
                    {
                        time: '2021-25-14',
                        money: 500,
                        historyСountMoney: 2814
                    }
                ]
            },
            {
                typeCart: 'credit',
                countMoney: 2500,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                creditLimit: 50000,
                creditCountMoney: 0,
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 256,
                        historyСountMoney: 2244
                    },
                    {
                        time: '2021-25-14',
                        money: 500,
                        historyСountMoney: 1744
                    }
                ]
            }
        ]
    },
    {
        id: 'c3c3d039-5732-4719-920c-d900c41a17d6',
        name: 'Виталик',
        surname: 'Ступка',
        isActive: false,
        registration: '20-11-2021',
        cart: [
            {
                typeCart: 'debit',
                countMoney: 4564,
                expairetAt: '20-11-2021',
                lastOperation: '20-11-2021',
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 256,
                        historyСountMoney: 4308
                    },
                    {
                        time: '2021-25-14',
                        money: 500,
                        historyСountMoney: 3808
                    }
                ]
            },
            {
                typeCart: 'credit',
                countMoney: 0,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                creditLimit: 40000,
                creditCountMoney: 10000,
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 2500,
                        historyСountMoney: 7500
                    },
                    {
                        time: '2021-25-14',
                        money: 1500,
                        historyСountMoney: 6000
                    },
                    {
                        time: '2021-25-15',
                        money: 1600,
                        historyСountMoney: 4400
                    }
                ]

            }
        ]
    },
    {
        id: '3d978a60-a851-4c6f-b9c8-635c7412207f',
        name: 'Света',
        surname: 'Назаренко',
        isActive: true,
        registration: '2021-10-11',
        cart: [
            {
                typeCart: 'debit',
                countMoney: 6564,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 350,
                        historyСountMoney: 6214
                    },
                    {
                        time: '2021-25-14',
                        money: 1500,
                        historyСountMoney: 4714
                    },
                    {
                        time: '2021-25-14',
                        money: 2500,
                        historyСountMoney: 2214
                    }
                ]

            },
            {
                typeCart: 'credit',
                countMoney: 0,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                creditLimit: 45000,
                creditCountMoney: 5000,
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 500,
                        historyСountMoney: 4500
                    },
                    {
                        time: '2021-25-14',
                        money: 1000,
                        historyСountMoney: 3500
                    }
                ]
            }
        ]
    },
    {
        id: 'ff594ab1-08e9-467b-a7c0-888fc23f9b3e',
        name: 'Жора',
        surname: 'Максимов',
        isActive: true,
        registration: '2021-10-11',
        cart: [
            {
                typeCart: 'debit',
                countMoney: 12000,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 2000,
                        historyСountMoney: 10000
                    },
                    {
                        time: '2021-25-14',
                        money: 1500,
                        historyСountMoney: 8500
                    }
                ]

            },
            {
                typeCart: 'credit',
                countMoney: 3500,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                creditLimit: 50000,
                creditCountMoney: 0,
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 220,
                        historyСountMoney: 3280
                    },
                    {
                        time: '2021-25-14',
                        money: 1500,
                        historyСountMoney: 1780
                    },

                    {
                        time: '2021-25-14',
                        money: 500,
                        historyСountMoney: 1280
                    }
                ]

            }
        ]
    },
    {
        id: '650d8d92-e364-4d17-87e0-4cb7aa681247',
        name: 'Степа',
        surname: 'Степанов',
        isActive: false,
        registration: '2021-10-11',
        cart: [
            {
                typeCart: 'debit',
                countMoney: 10500,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-120',
                        money: 1250,
                        historyСountMoney: 9250
                    },
                    {
                        time: '2021-25-14',
                        money: 1500,
                        historyСountMoney: 7750
                    }
                ]

            },
            {
                typeCart: 'credit',
                countMoney: 0,
                expairetAt: '2021-10-11',
                lastOperation: '2021-10-11',
                creditLimit: 30000,
                creditCountMoney: 20000,
                currency: 'USD',
                history: [
                    {
                        time: '2021-25-12',
                        money: 2500,
                        historyСountMoney: 17500
                    },
                    {
                        time: '2021-25-14',
                        money: 1700,
                        historyСountMoney: 15800
                    },

                    {
                        time: '2021-25-14',
                        money: 3500,
                        historyСountMoney: 12300
                    }
                ]
            }
        ]
    }
];